package app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.*;

public class GestioneAssicurazioni implements Serializable{
//attributi
private ArrayList <Assicurazione> listaAss = new ArrayList<Assicurazione>();

//costruttori
public GestioneAssicurazioni()
    {
            listaAss = new ArrayList<Assicurazione>();
    }
public GestioneAssicurazioni(ArrayList <Assicurazione> e)
    {
        this();
        for(Assicurazione a : e)
            {
                listaAss.add(a.clone());
            }
           
    }
 //loadCSV(String nomeFile)
 public void  loadCSV(String nomeFile)  throws  IOException
    {
        Scanner scanner = new Scanner(new File(nomeFile)).useDelimiter(System.lineSeparator());
        try 
        {
            while(scanner.hasNext())
                {
                    final String [] valori = scanner.next().split(",");
                    InserisciAssicurazione(new Assicurazione(valori[0], valori[1], valori[2],Double.valueOf(valori[3]), Double.valueOf(valori[4]), Double.valueOf(valori[5]), Double.valueOf(valori[6]),Double.valueOf(valori[7]), Double.valueOf(valori[8]), Double.valueOf(valori[9]), Double.valueOf(valori[10]), Double.valueOf(valori[12]), Double.valueOf(valori[12]), Double.valueOf(valori[13]), Double.valueOf(valori[14]), valori[15], valori[16], Integer.valueOf(valori[17]))); 
                   
                }
        } 
        catch (Exception e) {}
        finally
            {
                scanner.close();
            }
    }
 //save(String nomeFile) 
public void save(String nomeFile) throws IOException
    {
        final FileOutputStream f = new FileOutputStream(new File(nomeFile));
        ObjectOutputStream o = new ObjectOutputStream(f);

        o.writeObject(listaAss);
        f.close();
    }


public void load(String nomeFile) throws IOException
    {
        final FileInputStream fi = new FileInputStream(nomeFile);
        ObjectInputStream oi = new ObjectInputStream(fi);
        try 
            {
                this.listaAss = (ArrayList<Assicurazione>) oi.readObject();
            } 
        catch (Exception e) 
            {
                oi.close();
            }
    }
//metodo remove    
public void remove(Assicurazione a)
    {
        listaAss.remove(a);
    }
// sort() → ordinamento crescente per policyID
public void sort()
    {
        Collections.sort(listaAss);
    }
//metodo inserisci
public void InserisciAssicurazione(Assicurazione a)
    {
        listaAss.add(a);
    }
//metodo  edit(Assicurazione a) → il campo policyID è univoco per ogni assicurazione( chiave primaria). 
//metodod che cerca un'oggeto selezionato attraverso la policyID e va a sostitutire i vecchi valori( tranne policyID)attraverso quelli del nuovo oggetto.
/*public void edit(Assicurazione a)
{
    for(Assicurazione a: listaAss)
    {

    }
}*/

//metodo toString
@Override
public String toString()
    {
        String s="";
        for(Assicurazione a : listaAss)
            {
                s+=a.toString();
            }
        return s;
    }
}

package app;

import java.io.Serializable;

public class Assicurazione implements Serializable, Comparable<Assicurazione> {
//attributi
    private String polyciID;
    private String statecode;
    private String country;
    private double eq_site_limit;
    private double hu_site_limit;
    private double fl_site_limit;
    private double fr_site_limit;
    private double tiv_2011;
    private double tiv_2012;
    private double eq_site_deductible;
    private double hu_site_deductible;
    private double fl_site_deudctible;
    private double fr_site_deductible;
    private double point_latitude;
    private double point_longitude;
    private String line;
    private String construction;
    private int point_granularity;
//costruttori
public Assicurazione(){}
public Assicurazione(String polyciID,String statecode,String country,double eq_site_limit, double hu_site_limit, double fl_site_limit, double fr_site_limit, double tiv_2011, double tiv_2012, double eq_site_deductible, double hu_site_deductible, double fl_site_deudctible, double fr_site_deductible,double point_latitude,double point_longitude,String line,String construction,int point_granularity)
    {
        this.setPolyciID(polyciID);
        this.setStatecode(statecode);
        this.setCountry(country);
        this.setEq_site_limit( eq_site_limit);
        this.setHu_site_limit( hu_site_limit);
        this.setFl_site_limit( fl_site_limit);  
        this.setFr_site_limit( fr_site_limit) ;
        this.setTiv_2011( tiv_2011) ;
        this.setTiv_2012( tiv_2012) ;
        this.setEq_site_deductible( eq_site_deductible) ;
        this.setHu_site_deductible( hu_site_deductible);
        this.setFl_site_deudctible( fl_site_deudctible) ;
        this.setFr_site_deductible( fr_site_deductible) ;
        this.setPoint_latitude( point_latitude) ;
        this.setPoint_longitude( point_longitude) ;
        this.setLine( line) ;
        this.setConstruction( construction);
        this.setPoint_granularity( point_granularity);
    }
public Assicurazione(Assicurazione a)
    {
            this.setPolyciID(a.getPolyciID());
            this.setStatecode(a.getStatecode());
            this.setCountry(a.getCountry());
            this.setEq_site_limit( a.getEq_site_limit());
            this.setHu_site_limit( a.getHu_site_limit());
            this.setFl_site_limit( a.getFl_site_limit());  
            this.setFr_site_limit( a.getFr_site_limit()) ;
            this.setTiv_2011( a.getTiv_2011()) ;
            this.setTiv_2012( a.getTiv_2012()) ;
            this.setEq_site_deductible( a.getEq_site_deductible()) ;
            this.setHu_site_deductible( a.getHu_site_deductible());
            this.setFl_site_deudctible( a.getFl_site_deudctible()) ;
            this.setFr_site_deductible( a.getFr_site_deductible()) ;
            this.setPoint_latitude( a.getPoint_latitude()) ;
            this.setPoint_longitude( a.getPoint_longitude()) ;
            this.setLine( a.getLine()) ;
            this.setConstruction( a.getConstruction());
            this.setPoint_granularity( a.getPoint_granularity());
    }
//metodi get e set
    public String getPolyciID() {  return this.polyciID;}
    public void setPolyciID(String polyciID) {   this.polyciID = polyciID; }
    public String getStatecode() {    return this.statecode;}
    public void setStatecode(String statecode) {   this.statecode = statecode; }
    public String getCountry() {   return this.country;}
    public void setCountry(String country) {   this.country = country;}
    public double getEq_site_limit() {    return this.eq_site_limit; }
    public void setEq_site_limit(double eq_site_limit) {  this.eq_site_limit = eq_site_limit;}
    public double getHu_site_limit() {   return this.hu_site_limit;}
    public void setHu_site_limit(double hu_site_limit) {   this.hu_site_limit = hu_site_limit;}
    public double getFl_site_limit() {   return this.fl_site_limit;}
    public void setFl_site_limit(double fl_site_limit) {    this.fl_site_limit = fl_site_limit;}
    public double getFr_site_limit() {   return this.fr_site_limit;}
    public void setFr_site_limit(double fr_site_limit) {  this.fr_site_limit = fr_site_limit;}
    public double getTiv_2011() {    return this.tiv_2011;}
    public void setTiv_2011(double tiv_2011) {this.tiv_2011 = tiv_2011;}
    public double getTiv_2012() {    return this.tiv_2012;}
    public void setTiv_2012(double tiv_2012) {  this.tiv_2012 = tiv_2012;}
    public double getEq_site_deductible() { return this.eq_site_deductible;}
    public void setEq_site_deductible(double eq_site_deductible) {  this.eq_site_deductible = eq_site_deductible;}
    public double getHu_site_deductible() {   return this.hu_site_deductible;}
    public void setHu_site_deductible(double hu_site_deductible) {  this.hu_site_deductible = hu_site_deductible; }
    public double getFl_site_deudctible() {   return this.fl_site_deudctible;}
    public void setFl_site_deudctible(double fl_site_deudctible) {    this.fl_site_deudctible = fl_site_deudctible;}
    public double getFr_site_deductible() {   return this.fr_site_deductible;}
    public void setFr_site_deductible(double fr_site_deductible) {   this.fr_site_deductible = fr_site_deductible;}
    public double getPoint_latitude() { return this.point_latitude;}
    public void setPoint_latitude(double point_latitude) {    this.point_latitude = point_latitude;}
    public double getPoint_longitude() {    return this.point_longitude;}
    public void setPoint_longitude(double point_longitude) {   this.point_longitude = point_longitude;}
    public String getLine() {   return this.line;}
    public void setLine(String line) {    this.line = line;}
    public String getConstruction() {    return this.construction;}
    public void setConstruction(String construction) {    this.construction = construction;}
    public int getPoint_granularity() {  return this.point_granularity;}
    public void setPoint_granularity(int point_granularity) {this.point_granularity = point_granularity; }
//metodo equals
public boolean equals(Assicurazione a)
    {
        return this.getPolyciID().equals(a.getPolyciID()) && this.getStatecode().equals(a.getStatecode()) && this.getCountry().equals(a.getCountry()) && this.getEq_site_limit()==a.getEq_site_limit()&& this.getHu_site_limit()== a.getHu_site_limit()&& this.getFl_site_limit()==a.getFl_site_limit()&& this.getFr_site_limit()== a.getFr_site_limit()&&  this.getTiv_2011()==a.getTiv_2011() && this.getTiv_2012()== a.getTiv_2012() &&  this.getEq_site_deductible()==a.getEq_site_deductible() && this.getHu_site_deductible()==a.getHu_site_deductible() && this.getFl_site_deudctible()==a.getFl_site_deudctible() && this.getFr_site_deductible() ==a.getFr_site_deductible() && this.getPoint_latitude()== a.getPoint_latitude() && this.getPoint_longitude()== a.getPoint_longitude() &&  this.getLine().equals( a.getLine() ) && this.getConstruction().equals( a.getConstruction()) && this.getPoint_granularity()== a.getPoint_granularity();
    }
@Override
public boolean equals(Object obj)
    {
        if(obj instanceof Assicurazione)
            {
                return equals((Assicurazione)obj);
            }
        else
            {
                return false;
            }
    }
//metodo clone
@Override
public Assicurazione clone()
    {
        return new Assicurazione(this.getPolyciID(), this.getStatecode(), this.getCountry(), this.getEq_site_limit(), this.getHu_site_limit(),this.getFl_site_limit(), this.getFr_site_limit(), this.getTiv_2011(), this.getTiv_2012(), this.getEq_site_deductible(), this.getHu_site_deductible(), this.getFl_site_deudctible(), this.getFr_site_deductible(), this.getPoint_latitude(), this.getPoint_longitude(), this.getLine(), this.getConstruction(), this.getPoint_granularity());
    }
//metodo compareTo
@Override
public int compareTo(Assicurazione o) {
    // TODO Auto-generated method stub
     return this.getPolyciID().compareTo(o.getPolyciID());
}
@Override
public String toString() {
        return " polyciID='" + getPolyciID() + "'" +", statecode='" + getStatecode() + "'" +
        ", country='" + getCountry() + "'" +
            ", eq_site_limit='" + getEq_site_limit() + "'" +
            ", hu_site_limit='" + getHu_site_limit() + "'" +
            ", fl_site_limit='" + getFl_site_limit() + "'" +
            ", fr_site_limit='" + getFr_site_limit() + "'" +
            ", tiv_2011='" + getTiv_2011() + "'" +
            ", tiv_2012='" + getTiv_2012() + "'" +
            ", eq_site_deductible='" + getEq_site_deductible() + "'" +
            ", hu_site_deductible='" + getHu_site_deductible() + "'" +
            ", fl_site_deudctible='" + getFl_site_deudctible() + "'" +
            ", fr_site_deductible='" + getFr_site_deductible() + "'" +
            ", point_latitude='" + getPoint_latitude() + "'" +
            ", point_longitude='" + getPoint_longitude() + "'" +
            ", line='" + getLine() + "'" +
            ", construction='" + getConstruction() + "'" +
            ", point_granularity='" + getPoint_granularity() + "'" +System.lineSeparator();
    }

  

}
